import React from 'react';
import { FlatList, View, Text, Image, StyleSheet, ImageBackground } from 'react-native';

export default class Home extends React.Component {

  static navigationOptions = {
    title: 'Detail',
  };

  state = {
    data:[]
  };

  componentWillMount(){
    this.fetchData();
  }


  fetchData = async () => {
    const response = await fetch("https://flexinvest.me/drf/strategies", {
        method: "GET",
        headers: {
          'Token': this.props.navigation.state.params.token
        }
      });
    const json = await response.json();
    this.setState({data: json});
  };

  datos(token) {

      fetch("https://flexinvest.me/drf/strategies", {
        method: "GET",
        headers: {
          'Token': token
        }
      })
      .then((response) => response.text())
      .then((data) => {
        return data;
      })
      .done();
    }


  render() {
    const token = this.props.navigation.state.params.token;
    return (
      <View>
      <ImageBackground
        source={require('./img/background.jpg')}
        style={{width: '100%', height: '100%'}}
        >
          <FlatList
            data={this.state.data}
            keyExtractor={(x, i) => i}
            renderItem={({item}) =>
              <View style={styles.container}>
                <Text style={styles.textView}>
                  Name: {item.name}
                </Text>
                <View>
                  <Image source={{uri: item.thumbnail_150}}
                         style={styles.imageView}
                        />
                </View>
                <Text style={styles.textDescription}>
                  Description: {item.description}
                </Text>
              </View>
            }
          />
      </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: 'white',

  },
  textView: {
    color: 'white',
    fontSize: 20,
    fontWeight: "500",
    marginBottom: 20,
  },
  textDescription:{
    color: 'white',
    fontSize: 16,
  },
  imageView:{
    width: 150,
    height: 100
  },
  backgroundImage: {
      flex: 1,
      resizeMode: 'cover'
  }
});
