import React from 'react';
import { Component, StyleSheet, Text, View, AppRegistry, TextInput, TouchableHighlight, Alert, Button, ImageBackground } from 'react-native';

export default class Login extends React.Component {

  constructor(){
    super()

    this.state={
      user: '',
      pass: ''
    }

  }

  onRegisterPressed(){
    fetch('https://flexinvest.me/drf/auth/', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: this.state.user,
      password: this.state.pass,
    }),
  }).then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.token) {
        this.props.navigation.navigate('Home', {token: responseJson.token} );
      }else{
        alert("Error en los datos");
      }
    })
      .catch((error) => {
        console.error(error);
      });
  }

  changeUser(user){
    this.setState({user})
  }

  changePass(pass){
    this.setState({pass})
  }

  static navigationOptions = {
    title: 'Home title',
    header: null,
  };

  render() {
    return (
      <ImageBackground
        source={require('./img/background.jpg')}
        style={{width: '100%', height: '100%'}}
        >
            <View style={styles.container}>
                      <View style={styles.inputView}>
                        <TextInput
                          style={styles.inputStyle}
                          placeholder="User" value={this.state.user}
                          autoCapitalize="none"
                          placeholderTextColor="white"
                          onChangeText={(user) => this.changeUser(user)}
                         />
                       </View>
                       <View style={styles.inputView}>
                         <TextInput
                          style={styles.inputStyle}
                          placeholder="pass" value={this.state.pass}
                          secureTextEntry={true}
                          onChangeText={(pass) => this.changePass(pass)}
                         />
                      </View>
                      <TouchableHighlight onPress={this.onRegisterPressed.bind(this)} style={styles.buttonStyle}>
                        <Text style={styles.buttonTextStyle}>SEND</Text>
                      </TouchableHighlight>
            </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
   },
   inputView: {
        flex: 1,
        marginTop: 20,
        marginBottom: 10,
        maxHeight: 40
     },
    inputStyle: {
      width: 250,
      height: 40,
      borderBottomWidth: 1,
      borderColor: 'white',
      padding: 5,
      color: 'white'
    },
    buttonStyle: {
       borderWidth: 1,
       backgroundColor: 'white',
       borderRadius: 2,
       padding: 8,
       alignItems: 'center',
       justifyContent: 'center',
       width: 250
    },
    buttonTextStyle: {
        color: 'black'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover'
    }
});
