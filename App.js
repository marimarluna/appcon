import { StackNavigator } from 'react-navigation';

import Home from './src/Home';
import Login from './src/Login';

export default StackNavigator({
  Login: { screen: Login },
  Home: { screen: Home },
});
